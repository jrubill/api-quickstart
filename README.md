# API Quickstart Template

## Project Template for Rapid API Deployment

What I want to come out of the box for this:

1. Preconfigured Database Handling
2. Separation of Dev, Test, and Prod Configuration
3. Other Base Configurations I may want such as Authentication/Authorization
